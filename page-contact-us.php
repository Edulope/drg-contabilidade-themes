<?php
// Template Name: Contato
?>
	
<?php get_header(); ?>

<section class="contact-us">
	<div class="content">
		<h2>Fale Conosco</h2>
		<form>
			<div class="group-item">
				<label for="name">Nome:</label>
				<input type="text" id="name" name="name" placeholder="Ex: João da silva">
			</div>
			<div class="group-item">
				<label for="email">E-mail:</label>
				<input type="email" id="email" name="email" placeholder="Ex: joaodasilva@exemplo.com">
			</div>
			<div class="group-item">
				<label for="phone">Telefone:</label>
				<input type="text" id="phone" name="phone" placeholder="Ex: (99) 9999-9999">
			</div>
			<div class="group-item">
				<label for="message">Mensagem:</label>
				<textarea type="text" id="message" name="message" placeholder="Digite sua mensagem..."></textarea>
			</div>
			<button type="submit" class="btn">Enviar</button>
		</form>
	</div>
</section>

<?php get_footer(); ?>